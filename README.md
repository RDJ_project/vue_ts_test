
# vue + ts 测试项目模板

## 项目初始化安装依赖库
```
npm工具：npm run init
yarn工具：yarn
```

### 项目热重载调试启动
```
只代理dev服务器： npm run dev 或 yarn dev
启动mock假数据服务器：npm run dev:mock 或 yarn dev:mock （暂时没安装mock模块）
```

### 项目打包
```
npm run build 或 yarn build
```

### 项目测试 （暂时没安装测试模块）
```
npm run test 或 yarn test
```


### vite详细配置
官网 [配置文档](https://cli.vuejs.org/config/).

### 项目结构汇总
```
│  .browserslistrc --- 浏览器白名单（做浏览器兼容配置）
│  .eslintrc.js --- js或ts代码风格检测插件配置（配合编辑器插件使用，该项目配合pretter代码风格优化插件不需要自己注意代码风格自动优化） 
│  .eslintignore --- eslint忽略路径配置
│  .gitignore --- git忽略提交配置
│  postcss.config.js --- css代码转换工具配置（可以自动兼容浏览器版本或者利用插件进行样式单位转换，部分配置放在vue.config.js）
│  babel.config.js --- bable 代码转换版本兼容工具配置（有些库框架做成bable插件方便全局引用但是需要配置）
│  vue.config.js --- vite项目配置文件（vue做多了一层封装简化webpack配置）
│  cypress.json --- cypress端对端集成测试配置
│  jest.config.js --- 单元测试代码(jest)测试配置
│  package.json --- node npm模块及指令配置文件
│  README.md --- 说明文档
├─src --- 项目代码主工作目录
│   │  App.vue --- 根节点组件页面
│   │  main.ts  --- 入口文件
│   │  permission.ts  --- 全局路由守卫鉴权
│   ├─asset --- 静态资源
│   ├─directives --- 通用指令集
│   ├─layout --- 容器组件
│   ├─components  --- 常用组件、公用组件 （推荐把props格式抽离）
│   ├─dts --- 全局类型声明文件
│   ├─models --- 数据模型类 （表示数据格式及请求地址）
│   ├─views --- 页面文件目录
│   ├─router  --- 地址路由及路由守卫
│   ├─utils --- 常用工具 http等
│   ├─style --- 样式
│   ├─api --- api请求集
│   ├─store --- 前端数据状态管理器
│   ├─icons --- svg等图标资源管理
│   ├─lang --- 国际化
│   ├─pwa --- pwa应用代码
├─tests --- 测试代码工作目录
│   ├─e2e --- 端对端集成测试代码(cypress)
│   ├─├─ fixtures --- 数据文件目录(json)
│   ├─├─ plugins --- 插件及动态配置目录
│   ├─├─ results --- 测试结果导出 reporter目录
│   ├─├─ screenshots --- 测试截图导出目录
│   ├─├─ specs --- 测试代码目录
│   ├─├─ support --- 测试自定义扩展目录(扩展指令断言等)
│   ├─├─ videos --- 测试录屏导出目录
│   ├─├─ tsconfig.json --- 测试子项目的ts项目配置
│   ├─unit --- 单元测试代码(jest)
│   ├─├─ tsconfig.json --- 测试子项目的ts项目配置
│   ├─├─ xxx.jest.spec.xxx --- 测试代码文件
├─public  --- 公共资源
└─node_modules  --- 安装好的npm模块

```
### 项目整体结构

- vue全家桶
- elementUi pc ui框架
- sass 样式预编译框架
- webpack 自动化构建
- typescript 
- lodash js数据操作工具库
- moment 时间处理库
- crypto-js 加密库
- axios ajax请求库
- echarts 图表库
- path-to-regexp 路径处理 正则验证库
- vue-svgicon vue svg管理库
- class-transformer 数据模型类型转换库
- class-validator 数据模型类型验证库
- uuid uid工具库
- js-cookie cookie操作库
- cypress 端对端集成测试
- jest 单元测试
- vue/test-util vue测试套件
