/** @type {import('@ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  // preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  // preset: 'ts-jest',
  testEnvironment: "jsdom",
  rootDir: "./tests/unit",
  moduleFileExtensions: ["ts", "js", "json", "vue"],
  transform: {
    "^.+\\.(ts|js)$": "babel-jest",
    "^.+\\.vue$": "@vue/vue2-jest",
  },
};
