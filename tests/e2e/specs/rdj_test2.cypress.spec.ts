// rdj_test.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
import { cy, expect, describe, it } from "local-cypress";
describe("My First Test2", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "Welcome to Your Vue.js + TypeScript App");
    cy.get("h3").then((el) => {
      // console.log("el.text",el.text)
      cy.wrap(el.text()).should("be.istest");
      expect(el.text()).to.be.istest()
    }).screenshot();
    
  });
});
