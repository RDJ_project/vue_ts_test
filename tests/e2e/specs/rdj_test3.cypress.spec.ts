import { cy, expect, describe, it, assert } from "local-cypress";
describe("My First Test3", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "Welcome to Your Vue.js + TypeScript App");
    cy.intercept(
      {
        method: "POST", // Route all GET requests
        url: "/testAction", // that have a URL that matches '/users/*'
      },
      { fixture: "testRes.json" } // and force the response to be: []
    ).as("testAction"); // and assign an alias
    cy.get("button[data-test='testButton'").click();
    cy.wait("@testAction").then(({response,request}) => {
      console.log("interception.response?.body", response);
      if(response&&response.body)
      assert.isNotNull(response.body, "test action");
    });
  });
});
