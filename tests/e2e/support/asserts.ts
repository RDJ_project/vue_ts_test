/// <reference path="./index.d.ts" />

/**
 * Example that shows how to write a custom Chai assertion.
 *
 * @see https://www.chaijs.com/guide/helpers/
 * @example
 ```
  expect('var').to.be.istest()
  expect('var').to.not.be.istest()
  cy.wrap('var').should('be.istest')
  cy.wrap('var').should('not.be.istest')
```
 * */
const isTest = (_chai: Chai.ChaiStatic, utils:Chai.ChaiUtils) => {
    function assertisTest (this: Chai.AssertionStatic) {
      console.log("assertisTest",this._obj)
      this.assert(
        this._obj === 'Welcome to Your Vue.js + TypeScript App',
        'expected #{this} to be string "istest"',
        'expected #{this} to not be string "istest"',
        this._obj
      )
    }
  
    _chai.Assertion.addMethod('istest', assertisTest)
  }
  
  // registers our assertion function "isTest" with Chai
  chai.use(isTest)