declare namespace Cypress {
    interface Chainable {
      /**
       * 测试命令
       * @example cy.test('greeting')
       */
       test(value: string): Chainable<string>
    }

    interface Chainer<Subject> {
      /**
       * 测试断言
       *
       * @example
       ```
      expect('istest').to.be.istest()
      cy.wrap('istest').should('be.istest')
      ```
      * */
      (chainer: 'be.istest'): Chainable<Subject>
  
      /**
       * Custom Chai assertion that checks if given subject is NOT string "istest"
       *
       * @example
       ```
      expect('bar').to.not.be.istest()
      cy.wrap('bar').should('not.be.istest')
      ```
      * */
     (chainer: 'not.be.istest'): Chainable<Subject>
    }
  }

  declare namespace Chai {
    interface Assertion {
      /**
       * 测试断言
       *
       * @example
       ```
      expect('istest').to.be.istest()
      cy.wrap('istest').should('be.istest')
      ```
      * */
       istest(): Assertion
    }

    
  }
  