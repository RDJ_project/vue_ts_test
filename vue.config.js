const path = require("path");
const { name } = require('./package');
console.log("env",process.env.NODE_ENV)
module.exports = {
  // 开发生产环境
  publicPath:
    process.env.NODE_ENV === "production" // 项目根路径 分开发生产环境和正式环境
      ? "/" // 正式环境
      : "./",

  // 项目打包输出路径
  outputDir: "dist",

  // 打包文件名是否输出hash值
  filenameHashing: process.env.NODE_ENV === "production" 
  ? true 
  : false,

  // 代码 正式环境是否要source map （便于调试找到问题源码位置）
  productionSourceMap: false,

  // crossorigin:'', // html入口文件外部请求资源跨域设置
  css: {
    // 样式加载配置
    // requireModuleExtension: false, // 启用 CSS Modules 文件后缀添加.module视为使用
    sourceMap: true, // 样式文件 正式环境是否要source map
    loaderOptions: {
      postcss: {
        // 这里的选项会传递给 postcss-loader
        plugins: [
          require("postcss-pxtorem")({
            // px转rem插件
            rootValue: 16,
            unitPrecision: 5,
            propList: ["font", "font-size", "line-height", "letter-spacing"],
            selectorBlackList: [],
            replace: true,
            mediaQuery: false,
            minPixelValue: 0
          })
        ]
      },
      // 各种样式加载器loader内部配置
      // css: {
      //   // 这里的选项会传递给 css-loader
      // },
      // sass: {
      //   // @/ 是 src/ 的别名
      //   // 所以这里假设你有 `src/variables.sass` 这个文件
      //   // 注意：在 sass-loader v8 中，这个选项名是 "prependData"
      //   additionalData: `@import "~@/styles/index.scss";`
      // less: {
      //   // 注意不同版本配置不同
      //   // modifyVars: {
      //   //   "@ant-prefix": name
      //   // },
      //   // javascriptEnabled: true
      //   lessOptions: {
      //     javascriptEnabled: true,
      //     modifyVars: {
      //       // "@ant-prefix": name //antd样式前缀
      //       "@project-prefix": `${name}-`, //项目名前缀
      //       "@css-prefix": `ivu-` //ivew样式前缀在这设没用 iview前缀是半成品
      //     }
      //   }
      //   // additionalData: `@ant-prefix: ${name};`
      // }
    }
  },

  devServer: {
    port: 8080,
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    hot: true,
    disableHostCheck: true,
    overlay: {
      warnings: false,
      errors: false
    },
    progress: false,
    // before:function(app){
    //   app.get('/a')
    // },
    proxy: {
      "/proxy": {
        target: "https://192.168.39.167", //代理目标地址 dev服务器
        ws: true,
        secure: false, // 如果是https接口，需要配置这个参数 校验证书
        changeOrigin: true,
        pathRewrite: {
          "^/proxy": "" // proxy在代理时转义成空字符
        }
      },
    }
  },
  configureWebpack: {
    output: {
      filename: process.env.NODE_ENV === 'production' ? 'js/[name].[contenthash:10].js' : 'js/[name].[hash:10].js',
      library: `${name}-[name]`,
      libraryTarget: "umd",
      jsonpFunction: `webpackJsonp_${name}`,
    },
  },
  pluginOptions: {
    // "style-resources-loader": {
    //   //样式资源预加载插件
    //   preProcessor: "less",
    //   patterns: [path.resolve(__dirname, "./src/styles/index.less")]
    // },
    // codegen:{
    //   yamlPath:"./apiDoc",
    //   codePath:"src/openApi"
    // },
  },
  chainWebpack: config => {
    config.plugin("html").tap(args => {
      args[0].title = "vue ts 测试模板";
      return args;
    });
    // config.module
    //   .rule('images')
    //   .use('url-loader')
    //   .loader('url-loader')
    //   .tap(options => Object.assign(options, { limit: 10000 }));    
  },
};
